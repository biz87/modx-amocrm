<?php
$user=array(
  'USER_LOGIN'=>'*************',
 'USER_HASH'=>'************'
);

$subdomain='getnewlive'; 
$link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';

//Авторизуюсь
$response = amo($link, 'POST', $user);

//Ищу контакт по email
$link='https://'.$subdomain.'.amocrm.ru/api/v2/contacts/?query='.$email;
$response = amo($link);
$contact_id = 0;
if(count($response['_embedded']['items']) > 0){
    $contact_id = $response['_embedded']['items'][0]['id'];
}
if(!$contact_id){
    //Создаю контакт
    $link='https://'.$subdomain.'.amocrm.ru/api/v2/contacts/';
    $contacts['add']=array(
       array(
          'name' => $name,
          'custom_fields' => array(
             array(
                'id' => 326635,
                'values' => array(
                   array(
                      'value' => $phone,
                      'enum' => 720813
                   )
                )
             ),
             array(
                'id' => 326637,
                'values' => array(
                   array(
                      'value' => $email,
                      'enum' => 720823
                   )
                )
             ),
            
          )
       )
    );
    $response = amo($link, 'POST', $contacts);
    $contact_id = $response['_embedded']['items'][0]['id'];
}



$message_custom_field = [];
$form_name_custom_field = [];
if(!empty($message)){
    $message_custom_field = array(
        'id'=>552835,
        'values'=>array(
            array(
                'value'=>$message
            )
        )
    );
}
if(!empty($form_name)){
    $form_name_custom_field = array(
        'id'=>552849,
        'values'=>array(
            array(
                'value'=>$form_name
            )
        )
    );
}

//Создаю лида
$leads['add']=array(
  array(
    'name'=>$name,
    'created_at'=>time(),
    'status_id'=>$status,
    'pipeline_id' => $pipeline,
    'contacts_id' => $contact_id, 
    'custom_fields' => array(
        //Источник 
        array(
            'id'=>327049,
            'values'=>array(
                array(
                    'value'=>1137307
                )
            )
        ),
        $message_custom_field,
        $form_name_custom_field
    ),
  )
);

$link='https://'.$subdomain.'.amocrm.ru/api/v2/leads';
$response = amo($link, 'POST', $leads);

//Ставлю задачу менеджеру
$tasks['add']=array(
    array(
        'element_id'=>$response['_embedded']['items'][0]['id'], #ID сделки
        'element_type'=>2, #Показываем, что это - сделка, а не контакт
        'task_type'=>1, #Звонок
        'text'=>'Первичный контакт с сайта',
        'responsible_user_id'=>2137450,
        'complete_till_at'=>strtotime('23:59')
    ),
);

$link='https://'.$subdomain.'.amocrm.ru/api/v2/tasks';
$response = amo($link, 'POST', $tasks);

function amo($link, $type = 'GET', $fields = array()){
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    if($type == 'POST'){
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($fields));
    }
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); 
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); 
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl);
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    curl_close($curl);
    
    $code=(int)$code;
    $errors=array(
      301=>'Moved permanently',
      400=>'Bad request',
      401=>'Unauthorized',
      403=>'Forbidden',
      404=>'Not found',
      500=>'Internal server error',
      502=>'Bad gateway',
      503=>'Service unavailable'
    );
    try
    {
      if($code!=200 && $code!=204) {
        throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
      }
    }
    catch(Exception $E)
    {
      die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
    }
    
    $response=json_decode($out,true); 
    
    return $response;
}