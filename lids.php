<?php
$email = filter_input(INPUT_POST,'mail', FILTER_VALIDATE_EMAIL);
$name = trim( filter_input(INPUT_POST,'name',  FILTER_SANITIZE_STRING) );
$phone = trim( filter_input(INPUT_POST,'phone',  FILTER_SANITIZE_STRING) );
$book = trim( filter_input(INPUT_POST,'book',  FILTER_VALIDATE_INT) );
$pipeline = trim( filter_input(INPUT_POST,'pipeline',  FILTER_VALIDATE_INT) );
$status = trim( filter_input(INPUT_POST,'status',  FILTER_VALIDATE_INT) );
$form_name = trim( filter_input(INPUT_POST,'form_name',  FILTER_SANITIZE_STRING) );
$message = trim( filter_input(INPUT_POST,'text',  FILTER_SANITIZE_STRING) );

$errors = [];
if(empty($email) || !$email){
    $errors['email'] = 'email';
}
if(empty($phone) || !$phone){
    $errors['phone'] = 'phone';
}
if(empty($name) || !$name){
    $errors['name'] = 'name';
}

if(empty($book) || !$book || $book == 0){
    $errors['book'] = 'book';
}
if(empty($pipeline) || !$pipeline || $pipeline == 0){
    $errors['pipeline'] = 'pipeline';
}
if(empty($status) || !$status|| $status == 0){
    $errors['status'] = 'status';
}

if(count($errors) > 0){
    return $AjaxForm->error('Не заполнены обязательные поля', $errors);
}


//Работаю с AMOCRM
$modx->runSnippet('AMOCRM', array(
    'name' => $name, 
    'phone' => $phone, 
    'email' => $email, 
    'pipeline' => $pipeline,
    'status' => $status,
    'form_name' => $form_name,
    'message' => $message
));

return $AjaxForm->success('Заявка принята');